const escritores =[{"id":1,
                    "nombre":"Jorge Luis",
                    "apellido":"Borges",
                    "fechaDeNacimiento":"24/08/1899",
                    "libros":[{
                                "id":1,
                                "titulo":"Ficciones",
                                "descripcion":"Se trata de uno de sus mas...",
                                "anioDePublicacion":1944},
                                {
                                "id":2,
                                "titulo":"El Aleph",
                                "descripcion":"Recopilacion de cuentos...",
                                "anioDePublicacion":1949}
                                ]}]


function getAutores(req,res)
    {
        //Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
        let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Libreria</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";
        mensaje+="<h1>Autores</h1>"
        if(escritores.length>0)
            {
                for(const escritor of escritores)
                    {
                        //Por cada celular en el Array baja, agrego una lista con todos los datos del mismo a 'mensaje'.
                        mensaje+="<ul><li><b>Nombre: </b>"+escritor.nombre+".</li>"+"<li><b>Apellido: </b>"+escritor.apellido+".</li>"+"<li><b>Fecha de Nacimiento: </b>"+escritor.fechaDeNacimiento+".</li></ul>";
                    }       
            }
        else
            {
                mensaje+="<p>No hay autores registrados todavia.</p>"
            }

        //Le agrego a la variable mensaje el cierre del body y del html, despues de post todos los listados de gama alta, media y baja.
        mensaje+="</body></html";
        
        res.send(mensaje);
    }

function getAutor(req,res)
    {
        //Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
        let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Libreria</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";
        let exite = "";
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=escritor;
                    }            
            }       
    
        //Por cada celular en el Array baja, agrego una lista con todos los datos del mismo a 'mensaje'.
        mensaje+="<h1>Autor</h1><ul><li><b>Nombre: </b>"+existe.nombre+".</li>"+"<li><b>Apellido: </b>"+existe.apellido+".</li>"+"<li><b>Fecha de Nacimiento: </b>"+existe.fechaDeNacimiento+".</li></ul>";

        //Le agrego a la variable mensaje el cierre del body y del html, despues de post todos los listados de gama alta, media y baja.
        mensaje+="</body></html";
        
        res.send(mensaje);
    }


function getTodosLibrosAutor(req,res)
    {
        //Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
        let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Libreria</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";
        let exite;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=escritor;
                    }            
            }       
        
        mensaje+="<h1>Todos los libros de "+existe.nombre+" "+existe.apellido+":</h1>";

        for(const libro of existe.libros)
            {
                //Por cada celular en el Array baja, agrego una lista con todos los datos del mismo a 'mensaje'.
        mensaje+="<ul><li><b>Título: </b>"+libro.titulo+".</li>"+"<li><b>Descripción: </b>"+libro.descripcion+".</li>"+"<li><b>Año de publicación: </b>"+libro.anioDePublicacion+".</li></ul>";        
            }
        

        //Le agrego a la variable mensaje el cierre del body y del html, despues de post todos los listados de gama alta, media y baja.
        mensaje+="</body></html";
        
        res.send(mensaje);
    }

function getUnicoLibroAutor(req,res)
    {
        //Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
        let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Libreria</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";
        let exite;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=escritor;
                    }            
            }       
        
        mensaje+="<h1>El libro de "+existe.nombre+" "+existe.apellido+":</h1>";

        for(const libro of existe.libros)
            {
                if(libro.id === Number(req.params.idLibro))
                    {
                        mensaje+="<ul><li><b>Título: </b>"+libro.titulo+".</li>"+"<li><b>Descripción: </b>"+libro.descripcion+".</li>"+"<li><b>Año de publicación: </b>"+libro.anioDePublicacion+".</li></ul>";       
                    }   
            }        

        //Le agrego a la variable mensaje el cierre del body y del html, despues de post todos los listados de gama alta, media y baja.
        mensaje+="</body></html";
        
        res.send(mensaje);
    }

function postLibroAutor(req,res)
    {
        let exite;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=escritor;
                    }            
            }       

        existe.libros.push(req.body);
        
        res.send("<h1>Libro agregado de "+existe.nombre+" "+existe.apellido+".</h1>");
    }

function postAutores(req,res)
    {
        
        escritores.push(req.body);
        res.send("<h1>Autor agregado.</h1>");
    }


function deleteAutor(req,res)
    {
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=escritor;
                    }            
            }       
        escritores.pop(existe);
        res.send("<h1>El autor a sido eliminado del listado.</h1>");
    }

function deleteLibroAutor(req,res)
    { 
        let autor;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    { 
                        autor=escritor;
                    }            
            }

        let existe;
        for(const libro of autor.libros)
            {
                if(libro.id === Number(req.params.idLibro))
                    {
                        existe=libro;
                    }   
            }       
        
        autor.libros.pop(existe);
        res.send("<h1>El libro a sido eliminado.</h1>");
    }

function putAutor(req,res)
    { 

        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        escritor.nombre= req.body.nombre;
                        escritor.apellido= req.body.apellido;
                        escritor.fechaDeNacimiento= req.body.fechaDeNacimiento;
                        escritor.libros= req.body.libros;
                    }            
            }       
        
        res.send("<h1>El autor a sido actualizado.</h1>");
    }

function putLibroAutor(req,res)
    { 
        let autor;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    { 
                        autor=escritor;
                    }            
            }

        for(const libro of autor.libros)
            {
                if(libro.id === Number(req.params.idLibro))
                    {
                        libro.titulo= req.body.titulo;
                        libro.descripcion= req.body.descripcion;
                        libro.anioDePublicacion= req.body.anioDePublicacion;
                    }   
            }       
        
        res.send("<h1>El libro a sido actualizado.</h1>");
    }

function middlewareExisteAutorNombre(req,res,next)
    {
        let existe = false;
        for(const escritor of escritores)
            {
                if(escritor.nombre===req.body.nombre && escritor.apellido===req.body.apellido)
                    {
                        existe=true;
                    }
            }

        if(existe===true)
            {
                return res.status(404).send("<h1>El escritor ya existe.</h1>");
            }
        else
            {
                return next();
            }
        
    }

function middlewareExisteAutorId(req,res,next)
    {
        let existe = false;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        existe=true;
                    }            
            }

        if(existe===true)
            {
                return next();
            }
        else
            {
                return res.status(404).send("<h1>No se encontro al autor buscado.</h1>");
            }
    }

function middlewareExisteLibroId(req,res,next)
    {
        let autor;
        for(const escritor of escritores)
            {
                if(escritor.id === Number(req.params.id))
                    {
                        autor=escritor;
                    }            
            }

        let existe=false;
        for(const libro of autor.libros)
            {
                if(libro.id === Number(req.params.idLibro))
                    {
                        existe=true;
                    }           
            }

        if(existe==true)
            {
                return next();
            }
        else
            {
                return res.status(404).send("<h1>No se encontro el libro buscado.</h1>");           
            }
    }

function ListoElPollo()
    {
        console.log('A comer en el puerto 9090');
    }

module.exports =
    {
        "getAutores":getAutores,
        "getAutor":getAutor,
        "getTodosLibrosAutor":getTodosLibrosAutor,
        "getUnicoLibroAutor":getUnicoLibroAutor,
        "postAutores":postAutores,
        "postLibroAutor":postLibroAutor,
        "deleteAutor":deleteAutor,
        "deleteLibroAutor":deleteLibroAutor,
        "putAutor":putAutor,
        "putLibroAutor":putLibroAutor,
        "ListoElPollo":ListoElPollo,
        "middlewareExisteAutorNombre":middlewareExisteAutorNombre,
        "middlewareExisteAutorId":middlewareExisteAutorId,
        "middlewareExisteLibroId":middlewareExisteLibroId
    }
