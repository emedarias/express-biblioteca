//Importaciones
const funciones = require('./funciones.js');
const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const app = express();


const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Acamica API',
      version: '1.0.0'
    }
  },
  apis: ['./app.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(express.json());
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));


/**
 * @swagger
 * /autores/id/libros/idLibro:
 *  get:
 *    description: Llama a un unico libro de un autor por su id.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 */
app.get("/autores/:id/libros/:idLibro", funciones.middlewareExisteAutorId, funciones.middlewareExisteLibroId ,funciones.getUnicoLibroAutor);


/**
 * @swagger
 * /autores/id/libros:
 *  get:
 *    description: Llama a un unico autor por su id.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 */
app.get("/autores/:id/libros", funciones.middlewareExisteAutorId,funciones.getTodosLibrosAutor);

/**
 * @swagger
 * /autores/id:
 *  get:
 *    description: Llama a un unico autor por su id.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 */
app.get("/autores/:id", funciones.middlewareExisteAutorId,funciones.getAutor);

/**
 * @swagger
 * /autores:
 *  get:
 *    description: Llama a todo los autores.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 */
app.get("/autores",funciones.getAutores);

/**
 * @swagger
 * /autores:
 *  post:
 *    description: Crea un nuevo autor.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: nombre
 *      description: Nombre del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: fechaDeNacimiento
 *      description: Fecha de nacimiento del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: libros
 *      description: array con libros del autor
 *      in: raw
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post("/autores", funciones.middlewareExisteAutorNombre,funciones.postAutores);

/**
 * @swagger
 * /autores/id/libros:
 *  post:
 *    description: Crea un nuevo libro de un autor.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: titulo
 *      description: Titulo del libro.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Brebe descripcion de lo que trata el libro.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: anioDePublicacion
 *      description: Año en el que se publico el libro.
 *      in: raw
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post("/autores/:id/libros", funciones.middlewareExisteAutorId,funciones.postLibroAutor);

/**
 * @swagger
 * /autores/id:
 *  delete:
 *    description: Borra a un autor por su id.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 */
app.delete("/autores/:id", funciones.middlewareExisteAutorId,funciones.deleteAutor);

/**
 * @swagger
 * /autores/id/libros/idLibro:
 *  delete:
 *    description: Borra a un libro por su id.
 *    parameters:
 *    - name: id
 *      description: Id del autor.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: idLibro
 *      description: Id del libro autor.
 *      in: raw
 *      required: true
 *      type: string
 */
app.delete("/autores/:id/libros/:idLibro", funciones.middlewareExisteAutorId,funciones.middlewareExisteLibroId,funciones.deleteLibroAutor);

/**
 * @swagger
 * /autores/id:
 *  put:
 *    description: Actualiza a un autor existente.
 *    parameters:
 *    - name: id
 *      description: Id del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: nombre
 *      description: Nombre del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: fechaDeNacimiento
 *      description: Fecha de nacimiento del autor
 *      in: raw
 *      required: true
 *      type: string
 *    - name: libros
 *      description: array con libros del autor
 *      in: raw
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.put("/autores/:id", funciones.middlewareExisteAutorId,funciones.putAutor);

/**
 * @swagger
 * /autores/id/libros:
 *  put:
 *    description: Actualiza un libro.
 *    parameters:
 *    - name: id
 *      description: Id del libro.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: titulo
 *      description: Titulo del libro.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: Brebe descripcion de lo que trata el libro.
 *      in: raw
 *      required: true
 *      type: string
 *    - name: anioDePublicacion
 *      description: Año en el que se publico el libro.
 *      in: raw
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.put("/autores/:id/libros/:idLibro", funciones.middlewareExisteAutorId,funciones.middlewareExisteLibroId,funciones.putLibroAutor);

//Funcion para verificar que esta funcionando express, de ser asi, llama a la funcion ListoElPollo.
app.listen(9090, funciones.ListoElPollo);